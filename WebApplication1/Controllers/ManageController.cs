﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebApplication1.Models;
using System.Collections.Generic;
using System.Web.Security;
using WebApplication1.CustomAttribute;
using System.Globalization;

namespace WebApplication1.Controllers
{
    [UserAuthorizeAttribute]
    public class ManageController : Controller
    {
        // GET: /Manage/Index
        public ActionResult Index()
        {
            // Get DB record from authCookie
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            string userName = FormsAuthentication.Decrypt(authCookie.Value).Name;
            OnlineProjectManagementEntities db = new OnlineProjectManagementEntities();
            
            var model = new IndexViewModel
            {
                Logins = db.Employees.Where(u => u.AccountName == userName).ToList()
                
            };
            return View(model);
        }

        // GET: /Manage/Edit
        public ActionResult Edit()
        {
            // Get all DB record from Employees table
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            string userName = FormsAuthentication.Decrypt(authCookie.Value).Name;
            OnlineProjectManagementEntities db = new OnlineProjectManagementEntities();
            var cityList = getAllCities(db);
            var jobList = getAllJobs(db);
            var model = new ManageLoginsViewModel
            {
                Logins = db.Employees.ToList(),
                Cities = cityList,
                Jobs = jobList
            };

            
            return View(model);
        }
        
        // POST: /Manage/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ManageLoginsViewModel model)
        {
            // Get all DB record from Employees table
            OnlineProjectManagementEntities db = new OnlineProjectManagementEntities();
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.employeeId != null)
                    {
                        if (Request.Form["delFlag"] == "true")
                        {
                            deleteUser(db, model);
                            ViewBag.IsDelete = "true";
                        }
                        else
                        {
                            updateUser(db, model);
                        }
                    }

                    if (model.employeeId == null)
                    {
                        insertUser(db, model);
                    } 
                    
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    //ViewBag.Message = "Error while saving data. " + ex;
                    dbContextTransaction.Rollback();
                }
            }

            // Catch exception
            //catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            //{
            //    foreach (var entityValidationErrors in ex.EntityValidationErrors)
            //    {
            //        foreach (var validationError in entityValidationErrors.ValidationErrors)
            //        {
            //            Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
            //        }
            //    }
            //}

            var cityList = getAllCities(db);
            var jobList = getAllJobs(db);
           
            model.Logins = db.Employees.ToList();
            model.Cities = cityList;
            model.Jobs = jobList;

            return View(model);
        }

        protected List<SelectListItem> getAllCities(OnlineProjectManagementEntities db)
        {
            var cityList = new List<SelectListItem>();
             
            foreach (City city in db.Cities)
            {
                cityList.Add(new SelectListItem
                {
                    Value = city.CityID.ToString(),
                    Text = city.CityName
                });
            }
            return cityList;
        }

        protected List<SelectListItem> getAllJobs(OnlineProjectManagementEntities db)
        {
            var jobList = new List<SelectListItem>();
            foreach (Job job in db.Jobs)
            {
                jobList.Add(new SelectListItem
                {
                    Value = job.JobID.ToString(),
                    Text = job.Description
                });
            }
            return jobList;
        }

        protected void insertUser(OnlineProjectManagementEntities db, ManageLoginsViewModel model)
        {
            Employee employee = new Employee();
            employee.FirstName = model.firstName;
            employee.LastName = model.lastName;
            employee.AccountName = model.accountName;
            employee.Password = model.password;
            employee.DOB = convertDateTime(model.dob);
            employee.JobID = Int32.Parse(model.job);
            employee.CityID = Int32.Parse(model.city);
            employee.Status = 1;
            employee.Street = model.street;
            db.Employees.Add(employee);
            db.SaveChanges();
            ViewBag.Message = "Data saved successfully.";
        }

        protected void updateUser(OnlineProjectManagementEntities db, ManageLoginsViewModel model)
        {
            Employee employee = db.Employees.First(i => i.EmployeeID.ToString() == model.employeeId);
            employee.FirstName = model.firstName;
            employee.LastName = model.lastName;
            employee.AccountName = model.accountName;
            employee.Password = model.password;
            employee.DOB = convertDateTime(model.dob);
            employee.JobID = Int32.Parse(model.job);
            employee.CityID = Int32.Parse(model.city);
            employee.Status = 1;
            employee.Street = model.street;
            db.SaveChanges();
            ViewBag.Message = "Data saved successfully.";
        }

        protected void deleteUser(OnlineProjectManagementEntities db, ManageLoginsViewModel model)
        {
            Employee employee = db.Employees.First(i => i.EmployeeID.ToString() == model.employeeId);
            db.Employees.Remove(employee);
            db.SaveChanges();
            ViewBag.Message = "Data deleted successfully.";
        }

        protected DateTime convertDateTime(string datetimeString)
        {
            DateTime dt = DateTime.ParseExact(datetimeString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string format = "yyyy-MM-dd";
            return Convert.ToDateTime(dt.ToString(format));
        }
        ////
        //// GET: /Manage/AddPhoneNumber
        //public ActionResult AddPhoneNumber()
        //{
        //    return View();
        //}

        ////
        //// POST: /Manage/AddPhoneNumber
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    // Generate the token and send it
        //    var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
        //    if (UserManager.SmsService != null)
        //    {
        //        var message = new IdentityMessage
        //        {
        //            Destination = model.Number,
        //            Body = "Your security code is: " + code
        //        };
        //        await UserManager.SmsService.SendAsync(message);
        //    }
        //    return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        //}

        ////
        //// POST: /Manage/EnableTwoFactorAuthentication
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> EnableTwoFactorAuthentication()
        //{
        //    await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //    }
        //    return RedirectToAction("Index", "Manage");
        //}

        ////
        //// POST: /Manage/DisableTwoFactorAuthentication
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DisableTwoFactorAuthentication()
        //{
        //    await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //    }
        //    return RedirectToAction("Index", "Manage");
        //}

        ////
        //// GET: /Manage/VerifyPhoneNumber
        //public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        //{
        //    var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
        //    // Send an SMS through the SMS provider to verify the phone number
        //    return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        //}

        ////
        //// POST: /Manage/VerifyPhoneNumber
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
        //    if (result.Succeeded)
        //    {
        //        var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //        if (user != null)
        //        {
        //            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //        }
        //        return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
        //    }
        //    // If we got this far, something failed, redisplay form
        //    ModelState.AddModelError("", "Failed to verify phone");
        //    return View(model);
        //}

        ////
        //// GET: /Manage/RemovePhoneNumber
        //public async Task<ActionResult> RemovePhoneNumber()
        //{
        //    var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
        //    if (!result.Succeeded)
        //    {
        //        return RedirectToAction("Index", new { Message = ManageMessageId.Error });
        //    }
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //    }
        //    return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        //}

        ////
        //// GET: /Manage/ChangePassword
        //public ActionResult ChangePassword()
        //{
        //    return View();
        //}

        ////
        //// POST: /Manage/ChangePassword
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
        //    if (result.Succeeded)
        //    {
        //        var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //        if (user != null)
        //        {
        //            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //        }
        //        return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
        //    }
        //    AddErrors(result);
        //    return View(model);
        //}

        ////
        //// GET: /Manage/SetPassword
        //public ActionResult SetPassword()
        //{
        //    return View();
        //}

        ////
        //// POST: /Manage/SetPassword
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
        //        if (result.Succeeded)
        //        {
        //            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //            if (user != null)
        //            {
        //                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //            }
        //            return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
        //        }
        //        AddErrors(result);
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        ////
        //// GET: /Manage/ManageLogins
        //public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        //{
        //    ViewBag.StatusMessage =
        //        message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
        //        : message == ManageMessageId.Error ? "An error has occurred."
        //        : "";
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user == null)
        //    {
        //        return View("Error");
        //    }
        //    var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
        //    var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
        //    ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
        //    return View(new ManageLoginsViewModel
        //    {
        //        CurrentLogins = userLogins,
        //        OtherLogins = otherLogins
        //    });
        //}

        ////
        //// POST: /Manage/LinkLogin
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LinkLogin(string provider)
        //{
        //    // Request a redirect to the external login provider to link a login for the current user
        //    return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        //}

        ////
        //// GET: /Manage/LinkLoginCallback
        //public async Task<ActionResult> LinkLoginCallback()
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        //    }
        //    var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
        //    return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && _userManager != null)
        //    {
        //        _userManager.Dispose();
        //        _userManager = null;
        //    }

        //    base.Dispose(disposing);
        //}


    }
}